import secrets
from collections import deque
import multiprocessing
from bitstring import BitArray
import progressbar
import librosa, librosa.display
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import IPython.display as ipd
import numpy as np
import pandas as pd
import sklearn
from sklearn import preprocessing
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from skimage.transform import rescale, resize, downscale_local_mean
import cv2
from glob import glob
# from time import time
import time
import shutil
import os
import scipy
from scipy import signal
from scipy import stats
# from scipy.misc import imsave
import datetime
import math
from pathlib import Path
import youtube_dl
import argparse
import sys
import mido
import json
import os.path
from os import path
import seaborn as sns
import copy
import PIL
from scipy import misc

import random

from madmom.audio.filters import LogarithmicFilterbank
from madmom.features.onsets import SpectralOnsetProcessor
from madmom.audio.signal import normalize

# import parselmouth

from midiutil.MidiFile import MIDIFile
from pandas.core.frame import DataFrame

from multiprocessing import Pool
import multiprocessing as mp

from collections import deque
from pathlib import Path 
from sklearn.utils import shuffle


sns.set() # Use seaborn's default style to make attractive graphs


###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################        
class Channel:        
    def __init__(self):
        pass
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################         
    # def init_vars(self, light_pixel_interval=0.025,verbose=False,total_channels=6):
    #     self.total_channels=total_channels
    #     self.light_pixel_interval=light_pixel_interval
    #     self.verbose=verbose
    #     self.pattern_items = []
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def genPNG(self,dataroot,basename,channels,m=25.5):        
        saveName = '%s/%s_channels.png'%(dataroot, basename)    
        data = []
        for i,ch in enumerate(channels):
            ch_data = np.array(channels[ch]).reshape(1,-1)
            for j in range(5):
                ch_data = np.vstack((ch_data, ch_data))  
            if i == 0:
                data = copy.deepcopy(ch_data)
            else:
                data = np.vstack((data,copy.deepcopy(ch_data)))  
        data = np.array(data).astype('float32')
        data *= m # W:song's length, H:32 pixel
        cv2.imwrite(saveName, data)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def genNPZ(self,basename,dataroot='./light',total_channels=6):  
        pngPath = '%s/%s.png'%(dataroot,basename) 
        STR = basename.split('_', 5)
        song_name = STR[0]
        class_name = STR[1]
        pattern_type = STR[2]
        # print(song_name,class_name,pattern_type)

        an_image = PIL.Image.open(pngPath)
        # width, height = an_image.size
        # print(width, height)
        #  PIL.Image.NEAREST, PIL.Image.BOX, PIL.Image.BILINEAR, PIL.Image.HAMMING, PIL.Image.BICUBIC or PIL.Image.LANCZOS
        # an_image_sz = an_image.resize( (width, self.total_channels),  PIL.Image.NEAREST )

        # dim = (width, self.total_channels)
        # resize image
        # resized = cv2.resize(np.float32(an_image), dim, interpolation = cv2.INTER_AREA)

        # an_image_sz = an_image.resize( (width, self.total_channels),  PIL.Image.BICUBIC )
        # an_image_sz = an_image.resize( (width, self.total_channels),  PIL.Image.HAMMING )
        # an_image_sz = an_image.resize( (width, self.total_channels),  PIL.Image.BOX )
        # an_image_sz = an_image.resize( (width, self.total_channels),  PIL.Image.BILINEAR )


        # new_grayscale_image_sz = an_image_sz.convert("L")
        # new_grayscale_array_sz = np.asarray(new_grayscale_image_sz).astype('int')
        
        a = np.asarray(an_image)
        # print(a.shape)

        # new_grayscale_array_sz = np.asarray(an_image)

        # rows,cols = new_grayscale_array_sz.shape
        channels = dict()
        for ch in range(total_channels):
            channels['ch%d'%(ch+1)] = [int(round(x/25.5)) for x in a[ch*32+16]] 
            # channels['ch%d'%(ch+1)] = [round(x/25.5) for x in a[ch*32+16]] 
            # channels['ch%d'%(ch+1)] = [round(x/25.5) for x in a[ch*32]] 
        np.savez('%s/%s.npz'%(dataroot,song_name) , **channels) 

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def pattern_combination(self,keyword='**/S*_E*_B*.png',pattern_path='./pattern'):  
        datasetsPath = Path(pattern_path)
        links_path = datasetsPath.glob(keyword)
        L_list = []
        for link in links_path:
            L_list.append(link)
        L_list = shuffle(L_list)
        return L_list

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def effects_and(self,pngList,patternList,beats_n=5,tempo=120,mode='and',light_pixel_interval=0.025):  
        seconds_per_beat = 60/tempo
        print(seconds_per_beat,"beats/sec")
        pattern_items = deque(patternList)
        beats_w = int(beats_n*seconds_per_beat/light_pixel_interval)
        print(beats_w)
        
        pattern_data = []

        M = dict()
        for index, link in enumerate(pngList):
            M['array%d'%(index)] = cv2.imread(link)
            M['link%d'%(index)] = link

        light_total_width = int((M['array%d'%(0)].shape[1]//beats_w)*beats_w)
        print(light_total_width)

        for i in range(0,light_total_width,int(beats_w)):
            P = cv2.imread(str(pattern_items[0]))
            P = cv2.resize(P, (beats_w,192), interpolation=cv2.INTER_CUBIC)
            # ret,P = cv2.threshold(P,50,255,cv2.THRESH_BINARY)
            ret,P = cv2.threshold(P,50,255,cv2.THRESH_TOZERO)

            if i == 0:
                pattern_data = P
            else:
                pattern_data = np.hstack((pattern_data, P))
            
            # bitwiseNot = cv2.bitwise_not(crop_img,P)
            # bitwiseXor = cv2.bitwise_xor(crop_img,bitwiseNot)

            for ii in range(len(pngList)):
                if mode == 'and':
                    M['array%d'%(ii)][:, i:i+beats_w] = cv2.bitwise_and(M['array%d'%(ii)][:, i:i+beats_w],P,mask=None)
                elif mode == 'or':
                    M['array%d'%(ii)][:, i:i+beats_w] = cv2.bitwise_or(M['array%d'%(ii)][:, i:i+beats_w],P,mask=None)
            pattern_items.rotate(-1)
       
        for i in range(len(pngList)):
            m = cv2.cvtColor(M['array%d'%(i)], cv2.COLOR_BGR2GRAY)
            cv2.imwrite(M['link%d'%(i)],m)

        for ii in range(len(pngList)):
            dirname = os.path.dirname(M['link%d'%(ii)])
            basename = os.path.basename(M['link%d'%(ii)])
            STR = basename.split('_', 5)
            song_id = STR[0]
            class_name = STR[1]
            type_name=STR[2]
            m = cv2.cvtColor(pattern_data, cv2.COLOR_BGR2GRAY)
            filename = '%s/%s_%s_%s_pattern.png'%(dirname,song_id,class_name,type_name)
            print('output file name: ',filename)
            cv2.imwrite(filename,m)
        

        # import matplotlib.pyplot as plt
        # import warnings
        # def ignore_warn(*args, **kwargs):
        #     pass
        # warnings.warn = ignore_warn
        # %matplotlib inline
        # plt.figure(figsize=(40,20))

        # plt.subplot(4,1,1)
        # plt.xticks([]),plt.yticks([])
        # plt.axis('off')
        # plt.imshow(P)

        # plt.subplot(4,1,2)
        # plt.xticks([]),plt.yticks([])
        # plt.axis('off')
        # plt.imshow(M)

        # plt.subplot(4,1,3)
        # plt.xticks([]),plt.yticks([])
        # plt.axis('off')
        # plt.imshow(S)

        # # plt.subplot(4,1,4)
        # # plt.xticks([]),plt.yticks([])
        # # plt.axis('off')
        # # plt.imshow(img1_bg)

        # plt.show()
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def filter(self,src,mode,ksize=(21, 1),num=25.0,len=0,esize=(3,1),gsize=(21,1),dsize=(3,1),N=3): 
        res = 0
        #########################################################
        # 留尾
        if mode=='keep_tail': 
            kernel_ = np.array(([1, 0],
                                [0, -1],), dtype="float32")
            res = cv2.filter2D(src, -1, kernel_) 
        #########################################################
        # 留頭
        elif mode=='keep_head':
            kernel_ = np.array(([-1, 0],
                                [0, 1],), dtype="float32")
            res = cv2.filter2D(src, -1, kernel_) 
        #########################################################
        # 留中間
        elif mode=='keep_meddle':
            kernel_ = cv2.getStructuringElement(cv2.MORPH_CROSS, ksize)
            res = cv2.erode(src,kernel_,iterations=1)
        #########################################################
        elif mode=='erode':
            kernel_ = cv2.getStructuringElement(cv2.MORPH_CROSS, ksize)
            res = cv2.erode(src,kernel_,iterations=1)
            # res = cv2.GaussianBlur(res, (49, 1), 0)
        #########################################################
        elif mode=='dilate':
            kernel_ = cv2.getStructuringElement(cv2.MORPH_RECT,dsize)
            res = cv2.dilate(src,kernel_,iterations=1)
        #########################################################
        elif mode=='keep_head_tail_bulr':
            kernel_ = np.array(([1, 0],
                                [0, -1],), dtype="float32")
            res1 = cv2.filter2D(src, -1, kernel_) 
            
            kernel_ = np.array(([-1, 0],
                                [0, 1],), dtype="float32")
            res2 = cv2.filter2D(src, -1, kernel_) 
            
            kernel_ = cv2.getStructuringElement(cv2.MORPH_CROSS,ksize)
            res3 = cv2.erode(src,kernel_,iterations=1)

            # res4 = cv2.GaussianBlur(res3, ksize, 0)        
            res = res1 + res2 + res3
        #########################################################
        elif mode=='bulr':
            # kernel_ = cv2.getStructuringElement(cv2.MORPH_CROSS, (13,13))
            # res1 = cv2.erode(src,kernel_,iterations=1)

            # kernel_ = cv2.getStructuringElement(cv2.MORPH_CROSS, (9,9))
            # res2 = cv2.erode(src,kernel_,iterations=1)

            # kernel_ = cv2.getStructuringElement(cv2.MORPH_CROSS, (7,7))
            # res3 = cv2.erode(src,kernel_,iterations=1)

            if len == 0:
                res = cv2.GaussianBlur(src, gsize, 0)
            else:
               
                # # 提取图片像素值到矩阵
                # pixel_data = np.array(src)
                # # 提取目标区域
                # box_data = pixel_data[:, 0:len]

                tmp = copy.deepcopy(src)
                crop1 = tmp[:,:len]
                crop2 = tmp[:,-len:-1]

                res1 = cv2.GaussianBlur(crop1, gsize, 0)
                res2 = cv2.GaussianBlur(crop2, gsize, 0)
                tmp[:,:len] = res1
                tmp[:,-len:-1] = res2
                res = tmp
        #########################################################
        elif mode=='bulr-avg':
            avg_5x5 = np.ones(ksize, np.float32) / num
            res = cv2.filter2D(src, -1, avg_5x5)
        #########################################################
        elif mode=='lap-bulr':
            kernel = np.array(([0, 1, 0],
                                [1,-4, 1],
                                [0, 1, 0]), dtype="float32")
            res1 = cv2.filter2D(src, -1, kernel) 

            kernel_ = cv2.getStructuringElement(cv2.MORPH_CROSS, esize)
            res2 = cv2.erode(src,kernel_,iterations=1)

            res3 = cv2.GaussianBlur(res2, gsize, 0)

            res = res1+res3
        #########################################################
        elif mode=='bulr-erode':
            src[:,0] = 0
            src[:,-1] = 0
            src[0,:] = 0
            src[-1,:] = 0

            kernel_ = cv2.getStructuringElement(cv2.MORPH_RECT, esize)
            res1 = cv2.erode(src,kernel_,iterations=1)
            res2 = cv2.GaussianBlur(src, gsize, 0)
            kernel = np.array(([.0, .4, .0],), dtype="float32")
            res3 = cv2.filter2D(res2, -1, kernel) 

            res = res1+res3
        #########################################################
        elif mode=='lap':
            kernel = np.array(([0, 1, 0],
                                [1,-4, 1],
                                [0, 1, 0]), dtype="float32")
            res = cv2.filter2D(src, -1, kernel) 
        #########################################################
        elif mode=='erode-lap':
            
            src[:,0] = 0
            src[:,-1] = 0
            src[0,:] = 0
            src[-1,:] = 0
            
            kernel_ = cv2.getStructuringElement(cv2.MORPH_RECT, esize)
            res1 = cv2.erode(src,kernel_,iterations=1)
            kernel = np.array(([0, 1, 0],
                               [1,-4, 1],
                               [0, 1, 0]), dtype="float32")
        
            res = cv2.filter2D(res1, -1, kernel) 

        #########################################################
        elif mode=='lap48':
            src[:,0] = 0
            src[:,-1] = 0
            src[0,:] = 0
            src[-1,:] = 0
            
            kernel4 = np.array(([0, 1, 0],
                                [1,-4, 1],
                                [0, 1, 0]), dtype="float32")
            kernel8 = np.array(([-1, -1, -1],
                               [-1, 8, -1],
                               [-1, -1, -1]), dtype="float32")

            kernel = kernel4+kernel8

            
            res = cv2.filter2D(src, -1, kernel) 
        #########################################################
        elif mode=='lap-bulr':
            kernel = np.array(([0, 1, 0],
                                [1,-4, 1],
                                [0, 1, 0]), dtype="float32")
            res1 = cv2.filter2D(src, -1, kernel) 
            res2 = cv2.GaussianBlur(res1, gsize, 0)

            res = res2
        #########################################################
        elif mode=='lap2':
            kernel = np.array(( [-1,0, -1],), dtype="float32")
            res = cv2.filter2D(src, -1, kernel) 
        #########################################################
        elif mode=='g1':
            kernel = np.array(( [.5,.3, .2],), dtype="float32")
            res = cv2.filter2D(src, -1, kernel) 
        #########################################################
        elif mode=='8-conn':
            kernel = np.array(([-1, -1, -1],
                               [-1, 8, -1],
                               [-1, -1, -1]), dtype="float32")
            res = cv2.filter2D(src, -1, kernel) 
        #########################################################
        elif mode=='emboss':
            kernel = np.array(([-2,-1, 0],
                               [-1, 1, 1],
                               [ 0, 1, 2]), dtype="float32")
            res = cv2.filter2D(src, -1, kernel) 
        #########################################################
        elif mode=='sobel_135':
            kernel = np.array(([ 0,-1, 2],
                               [ 1, 0, -1],
                               [ 2, 1, 0]), dtype="float32")
            res = cv2.filter2D(src, -1, kernel) 
        #########################################################
        elif mode=='skeletonize':
            from skimage import morphology
            # img = np.array(img)  
            # Put threshold to make it binary
            binarr = np.where(src>128, 1, 0)
            
            res1 = morphology.skeletonize(binarr)
            res = np.where(res1>0, 255, 0)
        #########################################################
        elif mode=='skeletonize-dilate':
            from skimage import morphology
            # img = np.array(img)  
            # Put threshold to make it binary
            binarr = np.where(src>128, 1, 0)
            
            res1 = morphology.skeletonize(binarr)
            res = np.where(res1>0, 255, 0)

            kernel_ = cv2.getStructuringElement(cv2.MORPH_RECT,dsize)
            res = cv2.dilate(np.float32(res),kernel_,iterations=1)
        #########################################################
        elif mode=='bulr4':
            from skimage import morphology
            binarr = np.where(src>0, 1, 0)            
            sk_res = morphology.skeletonize(binarr)
            sk_res1 = np.where(sk_res>0, 255, 0)

            kernel_ = cv2.getStructuringElement(cv2.MORPH_CROSS,ksize)
            dilate_res = cv2.dilate(sk_res1,kernel_,iterations=1)

            Gaussian_res = cv2.GaussianBlur(dilate_res, (ksize[0]+2,1), 0)

            res = sk_res1 + Gaussian_res
        #########################################################
        elif mode=='fadein':
            h,w = src.shape

            # column, row = h, 2
            # A = [[0]*row for _ in range(column)]
            for i in range(h):
                src[i][:18]=self.moving_average(src[i][:20])
              
        #########################################################
        elif mode=='moving_average':
            from scipy.ndimage import uniform_filter 
#             h,w = src.shape
            
# #             aaa=self.moving_average(src[i])

#             column, row = h, 2
#             A = [[0]*row for _ in range(column)]

#             res1 = np.hstack((src, A))
#             bbb=[]
#             for i in range(h):
#                 aaa=self.moving_average(res1[i])
#                 bbb=np.vstack((aaa, bbb))

#             h1,w1 = bbb.shape
#             print(w,h)
#             print(w1,h1)

#             # res = res1[:,:-2]
        
            
            
            res = uniform_filter(src, size=N, mode='constant')

        return abs(res)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################      
    def moving_average(self, a, n=3):
        ret = np.cumsum(a, dtype=float)

        ret[n:] = ret[n:] - ret[:-n]

        # ans = ret[n - 1:] / n

        # return ans[:-(n-1)]
        return ret[n - 1:] / n
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def beat_channel(self,channels, basename, channel_info={'ch1':'onset','ch2':'beats','ch3':'plp'},light_pixel_interval=0.025):
        aaa = {}
        for i in channel_info:
            aaa[i] = []
        
        for name in channels:
            try:
                index=0
                level=0
                midi_note = np.load('midi/%s_%s.npz'%(basename,channel_info[name]))['midi_note']
                print(name,channel_info[name],len(midi_note),len(channels[name]))
                time = 0
                for i in range(len(channels[name])):
                    if index < len(midi_note) and time >= midi_note[index][0] and time < midi_note[index][1]:
                        level = 10
                        index += 1
                        counts = 0
                    else:
                        level = 0

                    if level > 10:
                        level = 10
                    elif level < 0:
                        level = 0
                    aaa[name].append(round(int(level)))   
                    time += light_pixel_interval
            except:
                aaa[name]=channels[name]        
        return aaa

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def genBackground(self, pngPath):
        src=cv2.imread(pngPath)
        print(src.shape)
        B_array = np.zeros((src.shape[0],src.shape[1]), np.float32)
        basename = os.path.basename(pngPath)
        STR = basename.split('_', 5)
        song_id = STR[0]
        cv2.imwrite('light/%s_melody_B_channels.png'%(song_id),B_array)
    
    
#     beat_channel = channel.beat_channel(channels=np.load('light/%s.npz'%(song_id)), basename='%s'%(song_id), channel_info={'ch1':'plp','ch2':'plp','ch3':'plp'})
#     channel.genPNG(dataroot='./light',basename='%s_melody_plp'%(song_id),channels=beat_channel)


#     for i in range(1000): #添加点噪声
#         y = np.random.randint(0,blur.shape[0])
#         x = np.random.randint(0,blur.shape[1])
#         w = 32
#         h = 32
#         blur = cv2.rectangle(blur, (x, y), (x + w, y + h), (0,0,0), -1)

#     cv2.imwrite('test.png', blur)