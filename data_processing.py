"""
[CE200] General-purpose training script for image-to-image translation.
    [Modified] Sample Rate form 22050 to 20442
    =============================
     Song Total Sec: 201.375 sec
    =============================
    - 201.375 use 22050(sample rate) => 4440321
    - Featurse Map Length: 4440321 / 512(hop length) = 8673, So that pixel time is 0.0232 sec (201.375/8673)
    -----------------------------------------
    - 201.375 use 20442(sample rate) => 4116510
    - Featurse Map Length: 4116510 / 512(hop length) = 8040
    - So, pixel time is 0.025 sec 
    ====================================================================
    [STEP 0] For Ground truth node to image
      * python data_processing.py --rule chord2img --csv ./train.csv 
    ====================================================================
    [STEP 1] For Train Datasets (generate train feature full png)
      * python data_processing.py --rule feature_full --csv ./train.csv --features_rule ver3
      * python data_processing.py --rule feature_full_encode --csv ./train.csv
    ====================================================================
    [STEP 2] For AB Features Datasets (combine Domain A&B for pix2pix inputs form train's feature full png)
      * python data_processing.py --rule AB_features --csv ./train.csv --shift --time_steps 0.4 --output_root ./datasets --hFlip
      * python data_processing.py --rule AB_features --csv ./train.csv --shift --time_steps 0.3 --output_root ./datasets --hFlip
      * python data_processing.py --rule AB_features --csv ./train.csv --shift --time_steps 0.1 --output_root ./datasets      
      * python data_processing.py --rule AB_features --csv ./train.csv --shift --time_steps 0.2 --output_root ./datasets       
    ====================================================================
    [STEP 3] For Train Datasets (spliting train's feature full png and output to root)
      * python data_processing.py --rule train --csv ./train.csv --test_size 0.001 --total_size 800000 --output_root ./datasets
    ====================================================================
    [STEP 4] For Test Datasets (generate test's feature full png)
      * python data_processing.py --rule feature_full --csv ./test.csv --features_rule ver3
    ====================================================================
    [STEP 5] For Test Datasets (cut test's feature full png)
      * python data_processing.py --rule cut_feature_full --csv ./test.csv
    ====================================================================



    [ALL_IN_ONE]
      * python data_processing.py --rule all_in_one --time_steps 0.5
by Gary Wang
"""
import os
import sys
import subprocess
import argparse
from pathlib import Path
from utils import Utils
import shutil
import json
import pandas as pd
from os import path

from multiprocessing import Pool
import multiprocessing as mp
import time
from datetime import datetime
import warnings
def ignore_warn(*args, **kwargs):
    pass
warnings.warn = ignore_warn

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################          
def run_copy_train_file(__path):
    desPath = '/'.join((utils.output_root, utils.train_dir))
    print('[COPY Train Datasets] %s -> %s '%(__path, desPath))
    shutil.copy(__path, desPath)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
def run_copy_val_file(__path):
    desPath = '/'.join((utils.output_root, utils.val_dir))
    print('[COPY Val Datasets] %s -> %s '%(__path, desPath))
    shutil.copy(__path, desPath )
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
def run_copy_test_file(__path):
    desPath = '/'.join((utils.output_root, utils.test_dir))
    print('[COPY Test Datasets] %s -> %s '%(__path, desPath))
    shutil.copy(__path, desPath )
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################          
def run_move_train_file(__path):
    desPath = '/'.join((utils.output_root, utils.train_dir))
    print('[MOVE Train Datasets] %s -> %s '%(__path, desPath))
    shutil.move(__path, desPath)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
def run_move_val_file(__path):
    desPath = '/'.join((utils.output_root, utils.val_dir))
    print('[MOVE Val Datasets] %s -> %s '%(__path, desPath))
    shutil.move(__path, desPath )
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
def run_move_test_file(__path):
    desPath = '/'.join((utils.output_root, utils.test_dir))
    print('[MOVE Test Datasets] %s -> %s '%(__path, desPath))
    shutil.move(__path, desPath )
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################  
def run_output_AB_features_datasets(imgs_MP3_Note):
    utils.run_output_AB_features_datasets(imgs_MP3_Note,hFlip=opt.hFlip)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################  
def run_chord2img(song_groundtruth_path):
    utils.run_chord2img(song_groundtruth_path)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################  
def run_Creating_Datasets_ALLINONE(song_path):  
    utils.run_Creating_Datasets_ALLINONE(song_path)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################  
def run_cut_features_image(song_path):  
    utils.run_cut_features_image(song_path[0],song_path[1])
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################  
def run_Creating_feature_full_encoder(song_path):  
    utils.run_Creating_feature_full_encoder(song_path)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################  
def run_result2note(song_id):    
    note_list = utils.result2note(song_id=song_id, result_dir=opt.result_dir)  
    return song_id, note_list

def _build_parser():
    parser = argparse.ArgumentParser()    
    parser.add_argument('--rule', required=True, help='AB_features, train, test, feature_full, feature_full_encode')
    parser.add_argument('--output_root', type=str, default='./datasets', help='path to images')
    parser.add_argument('--csv', default='train.csv', help='csv file (train.csv or test.csv)')
    parser.add_argument('--time_steps', type=float, default=0.1, help='time_steps for shift scale')
    parser.add_argument('--pixel_interval', type=float, default=0.025, help='time/pixel')
    parser.add_argument('--sample_rate', type=int, default=20442, help='sample_rate')    
    parser.add_argument('--test_size', type=float, default=0.25, help='splite datasets to train / val')
    parser.add_argument('--shift', action='store_true', help='shift time_steps in one song for features image')
    parser.add_argument('--renew_all', action='store_true', help='renew_all')
    parser.add_argument('--isTrain', action='store_true', help='isTrain')
    parser.add_argument('--hop_length', type=int, default=512, help='hop_length')
    parser.add_argument('--num_test', type=int, default=300000, help='num_test')
    parser.add_argument('--result_dir', type=str, default='./results/predict/images', help='result_dir')
    parser.add_argument('--eopch', type=str, default='latest', help='eopch')
    parser.add_argument('--total_size', type=int, default=10000, help='train/val/test total size')
    parser.add_argument('--startIndex', type=int, default=-1, help='stratIndex using for hooktheroy datasets')
    parser.add_argument('--endIndex', type=int, default=-1, help='endIndex using for hooktheroy datasets')  
    parser.add_argument('--datasetPath', type=str, default='../../lead-sheet-dataset/datasets/', help='for theroytab datasets path') 
    parser.add_argument('--avgRGB', type=float, default=-1.0, help='avgRGB<0 mean all pass, otherwise bypass smaller it')
    parser.add_argument('--hFlip', action='store_true', help='hFlip')
    parser.add_argument('--features_rule', type=str, default='ver2', help='features_rule ver1,ver2,ver3,ver4')
    
    return parser

def print_options(parser,opt):
    """Print and save options
    It will print both current options and default values(if different).
    It will save options into a text file / [checkpoints_dir] / opt.txt
    """
    message = ''
    message += '----------------- Options ---------------\n'
    for k, v in sorted(vars(opt).items()):
        comment = ''
        default = parser.get_default(k)
        if v != default:
            comment = '\t[default: %s]' % str(default)
        message += '{:>25}: {:<30}{}\n'.format(str(k), str(v), comment)
    message += '----------------- End -------------------'
    print(message)
            
if __name__ == '__main__':
    parser = _build_parser()
    opt, _ = parser.parse_known_args()
    
    print_options(parser,opt)
    utils = Utils()
    utils.init_vars(shift=opt.shift, hop_length=opt.hop_length, 
                    sample_rate=opt.sample_rate, time_steps=opt.time_steps, 
                    pixel_interval=opt.pixel_interval, isTrain=opt.isTrain,
                    renew_all=opt.renew_all,features_rule=opt.features_rule)
    utils.init_datasets(opt.output_root)
##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    if opt.rule == 'AB_features':
        startTime = time.time()
        train = pd.read_csv(opt.csv)
        imgs_MP3_Note = []
        ###################################################################################################
        for _path, _groundtruth, _songs in zip(train['paths'],train['groundtruth'],train['songs']):
            mp3_path = os.path.join(_path, _songs)
            basename = os.path.basename(mp3_path)
            imgname = '%s_feature_full.png'%(os.path.splitext(basename)[0])
            mp3_img_path = os.path.join(_path, imgname)

            groundtruth_path = os.path.join(_path, _groundtruth)
            basename = os.path.basename(groundtruth_path)
            imgname = '%s_%s.png'%(os.path.splitext(os.path.basename(mp3_path))[0],os.path.splitext(basename)[0])
            groundtruth_img_path = os.path.join(_path, imgname)

            if path.exists(mp3_img_path) == True and path.exists(groundtruth_img_path) == True:        
                print(mp3_img_path, groundtruth_img_path)
                imgs_MP3_Note.append((mp3_img_path, groundtruth_img_path))
            else:
                print(" Not found MP3(%s) or groundtruth(%s)"%(mp3_img_path,groundtruth_img_path))
        ###################################################################################################
        try:
            shutil.rmtree(utils.feature_file_path)
        except OSError as e:
            print(e)
        else:
            print("The %s directory is deleted successfully"%(utils.feature_file_path))
        os.makedirs('%s' % (utils.feature_file_path), exist_ok=True)    
        utils.delete_ipynb_checkpoints()
                
        ###################
        # Muti-processing #
        ###################
        pool_size = mp.cpu_count()
        print('pool size:%d'%(pool_size))
        pool = Pool(processes=pool_size)
        pool.map(run_output_AB_features_datasets, imgs_MP3_Note)
        pool.close()  
        pool.join()     
        print('Finishied %fs'%(time.time() - startTime))  

##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    elif opt.rule == 'train':
        import cv2
        import secrets
        import numpy as np
        import copy
        from glob import glob
        # from time import time
        import time
        import shutil
        # utils.output_train_datasets_theorytab(test_size=opt.test_size, total_size=opt.total_size)

        test_size=opt.test_size
        total_size=opt.total_size
        utils.secretsGenerator = secrets.SystemRandom()
        startTime = time.time()
        image_path_list = []

        # utils.output_train_datasets(test_size=opt.test_size, total_size=opt.total_size)
        for file_path in glob('%s/*.png'%(utils.feature_file_path)):
            image_path_list.append(file_path)

        if len(image_path_list) < total_size:
            total_size = len(image_path_list)
        # data_list = shuffle(image_path_list) 

        _train_size = int(total_size*(1-test_size))
        _test_size = int(total_size*(test_size)/2)
        _val_size = int(total_size*(test_size)/2)


        _train_size = int(total_size*(1-test_size))
        _test_size = int((total_size-_train_size)/2)
        _val_size = total_size-(_train_size+_test_size)
        print(_train_size,_test_size,_val_size)
        
        utils.secretsGenerator.shuffle(image_path_list) #.SystemRandom().shuffle(char_list)
        # all_train_list = utils.secretsGenerator.sample(data_list, k=int(test_size))

        train_list = image_path_list[0:_train_size]
        test_list = image_path_list[_train_size:_train_size+_test_size]
        val_list = image_path_list[_train_size+_test_size:] 


        print('=== Clean Train/Val/Test Datasets===')
        path_list = [utils.train_file_path , utils.val_file_path, utils.test_file_path]
        for pa in path_list:
            try:
                shutil.rmtree(pa)
            except OSError as e:
                print(e)
            finally:
                print("The %s directory is deleted successfully"%(pa))
            os.makedirs('%s' % (pa), exist_ok=True)
   
        utils.delete_ipynb_checkpoints()
        
        # Muti-processing
        print('### [%s] Creating Train/Val/Test Datasets For Pix2Pix ###'%(utils.output_root))  
        pool_size = mp.cpu_count()
        print('pool size:%d'%(pool_size))
        pool = Pool(processes=pool_size) # Pool() 不放參數則默認使用電腦核的數量  
#         pool.map(run_copy_val_file, val_list)
#         pool.map(run_copy_test_file, test_list)
#         pool.map(run_copy_train_file, train_list)
        pool.map(run_move_val_file, val_list)
        pool.map(run_move_test_file, test_list)
        pool.map(run_move_train_file, train_list)
        pool.close()  
        pool.join()  

        print("[%s] Row image size:"%(utils.feature_file_path), len(image_path_list))
        print("[%s] train:%d test:%d val:%d"%(utils.output_root, len(train_list), len(test_list), len(val_list)))
        print('=== Finishied %fs ==='%(time.time() - startTime)) 

##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    elif opt.rule == 'test':
        utils.output_test_datasets(csvFile=opt.csv)
##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    elif opt.rule == 'chord2img':    
        startTime = time.time()
        # utils.chord2img(opt.csv)   
        song_groundtruth_path = []
        train = pd.read_csv(opt.csv)
        count = 1
        for _path, _groundtruth, _songs in zip(train['paths'],train['groundtruth'],train['songs']):
            mp3_path = os.path.join(_path, _songs)
            groundtruth_path = os.path.join(_path, _groundtruth)
            if path.exists(mp3_path) == True:
                if path.exists(groundtruth_path) == True:   
                    song_groundtruth_path.append((_path, mp3_path, groundtruth_path))
                else:
                    print(" Not found Groundtruth %s"%(groundtruth_path))
            else:
                print(" Not found MP3 %s"%(mp3_path))  

        ###################
        # Muti-processing #
        ###################
        pool_size = mp.cpu_count()
        print('pool size:%d'%(10))
        pool = Pool(processes=10)   
        pool.map(run_chord2img, song_groundtruth_path)
        pool.close()  
        pool.join()     
        print('Finishied %fs'%(time.time() - startTime))  

##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    elif opt.rule == 'feature_full':             
        startTime = time.time()
        data = pd.read_csv(opt.csv)              
        files_MP3 = []
        for _path, _songs in zip(data['paths'],data['songs']):
            mp3_path = os.path.join(_path, _songs)
            if path.exists(mp3_path) == True:  
                files_MP3.append(mp3_path)          

        ###################
        # Muti-processing #
        ###################
        pool_size = mp.cpu_count()
        print('pool size:%d'%(10))
        pool = Pool(processes=10) 
        pool.map(run_Creating_Datasets_ALLINONE, files_MP3)
        pool.close()  
        pool.join()     
        print('Finishied %fs'%(time.time() - startTime))   

##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    elif opt.rule == 'cut_feature_full':             
        startTime = time.time()
        data = pd.read_csv(opt.csv)              
        files_MP3 = []
        for index,(_path, _songs) in enumerate(zip(data['paths'],data['songs'])):
            mp3_path = os.path.join(_path, _songs)
            if path.exists(mp3_path) == True:  
                files_MP3.append((index,mp3_path))

        ###################
        # Muti-processing #
        ###################
        pool_size = mp.cpu_count()
        print('pool size:%d'%(10))
        pool = Pool(processes=10) 
        pool.map(run_cut_features_image, files_MP3)
        pool.close()  
        pool.join()     
        print('Finishied %fs'%(time.time() - startTime))        

##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    elif opt.rule == 'feature_full_encode':             
        startTime = time.time()
        data = pd.read_csv(opt.csv)              
        files_MP3 = []
        for _path, _songs in zip(data['paths'],data['songs']):
            mp3_path = os.path.join(_path, _songs)
            if path.exists(mp3_path) == True:  
                files_MP3.append(mp3_path)          

        ###################
        # Muti-processing #
        ###################
        pool_size = mp.cpu_count()
        print('pool size:%d'%(10))
        pool = Pool(processes=10) 
        pool.map(run_Creating_feature_full_encoder, files_MP3)
        pool.close()  
        pool.join()     
        print('Finishied %fs'%(time.time() - startTime))      

##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    elif opt.rule == 'result2note':
        startTime = time.time()
        
        ###################
        # Muti-processing #
        ###################
        pool_size = mp.cpu_count()
        print('pool size:%d'%(pool_size))
        all_note_dict = {}
        songs_id = [x for x in range(1,1501)] 
        pool = Pool(processes=5)   
        s_id, s_list = zip(*pool.map(run_result2note, songs_id))     
        pool.close()  
        pool.join()     
        
        ###############
        # Output json #
        ###############
        all_note_dict = {}
        for _id, _list in zip(s_id, s_list):
            all_note_dict['%d'%(_id)] = _list  
        datestr = datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
        with open('result-%s.json'%(datestr), 'w') as outfile:
            json.dump(all_note_dict, outfile)              
        print('Finishied %fs'%(time.time() - startTime))


##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    elif opt.rule == 'all_in_one':  
        import copy     
        import random
        import numpy as np      
        from PIL import Image
        from numpy import asarray
        from sklearn import preprocessing
        startTime = time.time()
        ##########################################################################################
        ##########################################################################################
        songs_path = Path(utils.source_data_dir_path).glob('*/*.mp3')         
        # all_data=np.load('all_data.npy')
        all_data={}
        counts=1
        ##########################################################################################
        ##########################################################################################
        print('### Get waveform/GT ###')
        for p in songs_path:
            # print('\r'+f' Processed {counts}/{len(list(songs_path))} waveforms',end='')
            # print('[%s]'%(str(p)))
            dir_name = os.path.dirname(str(p))
            base_name = os.path.basename(str(p))
            song_id = (os.path.splitext(base_name)[0])
            print('[%d][%s]'%(counts,str(p)))


      
            chord_path=dir_name+'/%s_ground_truth.png'%(song_id)
            image = Image.open(chord_path)
            data = asarray(image)

            all_data[song_id] = {
                'song_id':song_id,
                'song_path':str(p),
                'chord_path':chord_path,
                'waveform':copy.deepcopy(np.array(utils.get_waveforms(p))),
                'chord':copy.deepcopy(data),
                'features':[],
            }

            # if counts>5:
            #     break
            counts += 1


        #     # print('\r'+f' Processed {counts}/{len(list(songs_path))} waveforms',end='')
        ##########################################################################################
        ##########################################################################################
        print('### Spile Train/Val/Test ###')
        random.seed(109)
        l = list(all_data.items())
        random.shuffle(l)
        all_data = dict(l)

        all_indices = all_data.keys()
        all_indices=[*all_indices]

        # # store dim (length) of the emotion list to make indices
        dim = len(all_indices)

        # store indices of training, validation and test sets in 80/10/10 proportion
        # train set is first 80%
        train_indices = all_indices[:int(0.8*dim)]
        # validation set is next 10% (between 80% and 90%)
        valid_indices = all_indices[int(0.8*dim):int(0.9*dim)]
        # test set is last 10% (between 90% - end/100%)
        test_indices = all_indices[int(0.9*dim):]

        print(train_indices,valid_indices,test_indices)
        ##########################################################################################
        ##########################################################################################
        print('### Get Features ###')
        for key, value in all_data.items():
            # print(key,'--->',value)
            utils.get_features(value['waveform'],value['features'])
            value['features'] = value['features'][0]
            scaler = preprocessing.MaxAbsScaler()
            value['norm_features']=scaler.fit_transform(np.array(value['features']))


        # print(np.array(all_data['95']['features']).shape)
        # print(np.array(all_data['30007']['features']).shape)
        # print(np.array(all_data['95']['norm_features']).shape)
        

        ##########################################################################################
        ##########################################################################################
        print('### Gen Train/Val/Test Shitf:%.2fs###'%(opt.time_steps))
        X_train,X_valid,X_test = [],[],[]
        y_train,y_valid,y_test = [],[],[]
        print(' -> train_indices -> X_train/y_train')
        for i in train_indices:
            feature = all_data[i]['norm_features']
            gt = all_data[i]['chord']
            for j in range(0,feature.shape[1],int(opt.time_steps/opt.pixel_interval)):
                X_train.append(feature[:,j:256+j])
                y_train.append(gt[:,j:256+j,:])
        
                if np.array(X_train[-1]).shape[1] < 256:
                    row_size = np.array(X_train[-1]).shape[0]
                    pad_size = np.array(X_train[-1]).shape[1]
                    features_homo = np.zeros((row_size,256))
                    features_homo[:,:pad_size] = X_train[-1][:,:pad_size]   
                    X_train[-1] = features_homo

                    row_size = y_train[-1].shape[0]
                    gt_homo = np.zeros((row_size,256,3))
                    gt_homo[:,:pad_size,:] = y_train[-1][:,:pad_size,:]   
                    y_train[-1] = gt_homo
        #--------------------------------------------------------
        print(' -> valid_indices -> X_valid/y_valid')
        for i in valid_indices:
            feature = all_data[i]['norm_features']
            gt = all_data[i]['chord']
            for j in range(0,feature.shape[1],int(opt.time_steps/opt.pixel_interval)):
                X_valid.append(feature[:,j:256+j])
                y_valid.append(gt[:,j:256+j,:])
        
                if X_valid != [] and np.array(X_valid[-1]).shape[1] < 256:
                    row_size = np.array(X_valid[-1]).shape[0]
                    pad_size = np.array(X_valid[-1]).shape[1]
                    features_homo = np.zeros((row_size,256))
                    features_homo[:,:pad_size] = X_valid[-1][:,:pad_size]   
                    X_valid[-1] = features_homo

                    row_size = y_valid[-1].shape[0]
                    gt_homo = np.zeros((row_size,256,3))
                    gt_homo[:,:pad_size,:] = y_valid[-1][:,:pad_size,:]   
                    y_valid[-1] = gt_homo
        #----------------------------------------------------------
        print(' -> test_indices -> X_test/y_test')
        for i in test_indices:
            feature = all_data[i]['norm_features']
            gt = all_data[i]['chord']
            for j in range(0,feature.shape[1],int(opt.time_steps/opt.pixel_interval)):
                X_test.append(feature[:,j:256+j])
                y_test.append(gt[:,j:256+j,:])
        
                if X_test != [] and np.array(X_test[-1]).shape[1] < 256:
                    row_size = np.array(X_test[-1]).shape[0]
                    pad_size = np.array(X_test[-1]).shape[1]
                    features_homo = np.zeros((row_size,256))
                    features_homo[:,:pad_size] = X_test[-1][:,:pad_size]   
                    X_test[-1] = features_homo

                    row_size = y_test[-1].shape[0]
                    gt_homo = np.zeros((row_size,256,3))
                    gt_homo[:,:pad_size,:] = y_test[-1][:,:pad_size,:]   
                    y_test[-1] = gt_homo

        print(X_train[0].shape)
        print(X_train[1].shape)
        print(len(X_train))


        print('### Clean TrainAB/ValAB/TestAB Datasets ###')
        path_list = [utils.train_file_path , utils.val_file_path, utils.test_file_path]
        for pa in path_list:
            try:
                shutil.rmtree(pa+'A')
                shutil.rmtree(pa+'B')
            except OSError as e:
                print(e)
            finally:
                print("The %s directory is deleted successfully"%(pa))
            os.makedirs('%s' % (pa+'A'), exist_ok=True)
            os.makedirs('%s' % (pa+'B'), exist_ok=True)
   
        # utils.delete_ipynb_checkpoints()


        ###### SAVE #########
        print('### SAVE Train/Val/Test ###')
        for index,(X_data,Y_data) in enumerate(zip(X_train,y_train)):
            filenameA = utils.train_file_path+'A/%d.npy'%(index)
            filenameB = utils.train_file_path+'B/%d.npy'%(index)
            np.save(filenameA, X_data)
            np.save(filenameB, Y_data)
            print(f'[TrainAB] Features and labels saved to {filenameA}/{filenameB}')

        for index,(X_data,Y_data) in enumerate(zip(X_valid,y_valid)):
            filenameA = utils.val_file_path+'A/%d.npy'%(index)
            filenameB = utils.val_file_path+'B/%d.npy'%(index)
            np.save(filenameA, X_data)
            np.save(filenameB, Y_data)
            print(f'[ValAB] Features and labels saved to {filenameA}/{filenameB}')

        for index,(X_data,Y_data) in enumerate(zip(X_test,y_test)):
            filenameA = utils.test_file_path+'A/%d.npy'%(index)
            filenameB = utils.test_file_path+'B/%d.npy'%(index)
            np.save(filenameA, X_data)
            np.save(filenameB, Y_data)
            print(f'[TestAB] Features and labels saved to {filenameA}/{filenameB}')

        # self.train_file_path = '/'.join((output_root, self.train_dir))
        # self.val_file_path = '/'.join((output_root, self.val_dir))
        # self.test_file_path = '/'.join((output_root, self.test_dir))          
        
        # # concatenate, in order, all waveforms back into one array 
        # X_train = np.concatenate(X_train,axis=0)
        # X_valid = np.concatenate(X_valid,axis=0)
        # X_test = np.concatenate(X_test,axis=0)

        # # concatenate, in order, all emotions back into one array 
        # y_train = np.concatenate(y_train,axis=0)
        # y_valid = np.concatenate(y_valid,axis=0)
        # y_test = np.concatenate(y_test,axis=0)

        # check shape of each set
        # print(f'Training waveforms:{X_train.shape}, y_train:{y_train.shape}')
        # print(f'Validation waveforms:{X_valid.shape}, y_valid:{y_valid.shape}')
        # print(f'Test waveforms:{X_test.shape}, y_test:{y_test.shape}')



        # Save
        np.save('all_data.npy', all_data) 



        # ###################
        # # Muti-processing #
        # ###################
        # pool_size = mp.cpu_count()
        # print('pool size:%d'%(10))
        # pool = Pool(processes=10) 
        # pool.map(run_Creating_feature_full_encoder, files_MP3)
        # pool.close()  
        # pool.join()     
        print('Finishied %fs'%(time.time() - startTime)) 

    sys.exit(0)
    
    